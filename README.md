# **Plaster**
A fast linear pan-genome construction software

## **Usage**

Plaster can be installed via the bioconda channel:

```
conda install -c bioconda pan-plaster
```
If you wish to use the source code, you must have [Biopython](https://github.com/biopython/biopython), [tqdm](https://github.com/tqdm/tqdm) and [MUMmer4.0](https://github.com/mummer4/mummer)

```
usage: plaster [-h] [-r] [-t TEMPLATE] [-o OUTPUT] [-w WORK_DIR] [-p THREADS]
               [-l LENGTH] [-f MAX_FRAG_LEN] [-i ID_CUTOFF] [-v]
               input-files [input-files ...]

positional arguments:
  input-files           a list of input fasta file names. If there is one
                        file, it is assumed that this file contains a list of
                        input files separated by a newline

optional arguments:
  -h, --help            show this help message and exit
  -r, --realign         Realign all input genomes to the resulting pangenome
                        to get a more accurate fragment mapping
  -t TEMPLATE, --template TEMPLATE
                        seed genome to use
  -o OUTPUT, --output OUTPUT
                        output pan-genome fasta and metadata file stem (does
                        not include file extension)
  -w WORK_DIR, --work-dir WORK_DIR
                        Directory to save nucmer outputs.
  -p THREADS, --threads THREADS
                        Number of threads
  -l LENGTH, --length LENGTH
                        Minimum length of sequence attached to the pan-genome
  -f MAX_FRAG_LEN, --max-frag-len MAX_FRAG_LEN
                        Maximum fragment length
  -i ID_CUTOFF, --id-cutoff ID_CUTOFF
                        Minimum identity to record alignment in metadata
  -v, --verbose         Print verbose output
```

### Example:

The following command will run `plaster` on each file listed in `exp_input.txt` and output two files:
`exp_pangenome.fasta` and `exp_pangenome.tsv`
```
./plaster example/exp_input.txt -o exp_pangenome
```

Alternatively, if you wish to run plaster on all files in some directory, you can pass in a list of files:
```
./plaster example/*.fasta -o exp_pangenome
```

To generate a more accurate mapping from input genomes to fragments, you will have to realign the input 
to the final pangenome. Note that this will not change the final pangenome, but instead just add more alignments
to the metadata file. For convenience, you can use the `-r`, `--realign` flag to rerun the alignment.


### **Pipeline**

![](image/Pipeline.png)


## **Citing Plaster**

If you use Plaster in your project, please cite: 

> Wang, Q., Elworth, R. A., Liu, T. R., & Treangen, T. J. (2019). 
Faster Pan-Genome Construction for Efficient Differentiation of Naturally Occurring 
and Engineered Plasmids with Plaster. 
In 19th International Workshop on Algorithms in Bioinformatics (WABI 2019). 
Schloss Dagstuhl-Leibniz-Zentrum fuer Informatik.
